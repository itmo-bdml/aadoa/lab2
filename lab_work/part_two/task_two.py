import random

import numpy as np
import matplotlib
import matplotlib.pyplot as plt
from lab_work.part_two.search_algos import mse_loss, exhaustive_search, gauss_method

from scipy.optimize import minimize

matplotlib.use('TkAgg')

random.seed(42)
np.random.seed(42)
EPSILON = 0.001


def linear(x, a: float, b: float):
    return a * x + b


def rational(x, a: float, b: float):
    return a / (1 + b * x)


a = np.random.uniform()
b = np.random.uniform()
# a = random.uniform(0, 1)
# b = random.uniform(0, 1)
x = np.arange(0, 101, 1) / 100
y = np.array(a * x + b + np.random.normal(size=101))


def mse_loss_linear(smth):
    a, b = smth
    return mse_loss(linear, a, b, x, y)


def mse_loss_rational(smth):
    a, b = smth
    return mse_loss(rational, a, b, x, y)


# print(x)
print("a=%.2f, b=%.2f" % (a, b))

base_loss = 0
print(f"base loss: {mse_loss(linear, a, b, x, y)}")


a1_linear, b1_linear, loss1_linear, iters1_linear, fcalcs1_linear = exhaustive_search(f=linear, loss=mse_loss, x=x, y=y)
a1_rational, b1_rational, loss1_rational, iters1_rational, fcalcs1_rational = exhaustive_search(f=rational, loss=mse_loss, x=x, y=y)
print(f"\n------ Exhaustive search ------")

print("linear exhaustive: f(x)=%.2fx + %.2f, loss = %.4f, iterations=%d, f-calculations=%d" % (a1_linear, b1_linear, loss1_linear, iters1_linear, fcalcs1_linear))
print("rational exhaustive: f(x) = %.2fx / (1 + %.2fx), loss = %.4f, iterations=%d, f-calculations=%d" % (a1_rational, b1_rational, loss1_rational, iters1_rational, fcalcs1_rational))

a2_linear, b2_linear, loss2_linear, iters2_linear, fcalcs2_linear = gauss_method(f=linear, loss=mse_loss, x=x, y=y)
a2_rational, b2_rational, loss2_rational, iters2_rational, fcalcs2_rational = gauss_method(f=rational, loss=mse_loss, x=x, y=y)


print(f"\n------ Gauss's method ------")

# print("rational Gauss", a2_linear, b2_linear, loss2_linear)
print("linear Gauss's method: f(x)=%.2fx + %.2f, loss = %.4f, iterations=%d, f-calculations=%d" % (a2_linear, b2_linear, loss2_linear, iters2_linear, fcalcs2_linear))
print("rational Gauss's method: f(x) = %.2fx / (1 + %.2fx), loss = %.4f, iterations=%d, f-calculations=%d" % (a2_rational, b2_rational, loss2_rational, iters2_rational, fcalcs2_rational))

print(f"\n------ Nelder-Mead method ------")

nelder_linear = minimize(mse_loss_linear, [0., 0.], method='Nelder-Mead', tol=1e-3,
                         options={'disp': False, 'xatol': 1e-3, 'fatol': 1e-3})
nelder_rational = minimize(mse_loss_rational, [0., 0.], method='Nelder-Mead', tol=1e-3,
                           options={'disp': False, 'xatol': 1e-3, 'fatol': 1e-3})
# print(nelder_linear)
a3_linear, b3_linear = nelder_linear.x
a3_rational, b3_rational = nelder_rational.x

print("linear Nelder-Mead: f(x)=%.2fx + %.2f, loss = %.4f, iterations=%d, f-calculations=%d" % (a3_linear, b3_linear, nelder_linear.fun, nelder_linear.nit, nelder_linear.nfev))
print("rational Nelder-Mead: f(x) = %.2fx / (1 + %.2fx), loss = %.4f, iterations=%d, f-calculations=%d" % (a3_rational, b3_rational, nelder_rational.fun, nelder_rational.nit, nelder_rational.nfev))
plt.figure(figsize=(12, 6))

plt.subplot(121)
plt.title("linear")
plt.scatter(x, y)
plt.plot(x, linear(x, a, b), label="expected linear")
plt.plot(x, a1_linear * x + b1_linear, label="predicted exhaustive")
plt.plot(x, a2_linear * x + b2_linear, label="predicted gauss")
plt.plot(x, a3_linear * x + b3_linear, label="predicted nelder")
plt.legend(loc="best")

plt.subplot(122)
plt.scatter(x, y)
plt.title("rational")
plt.plot(x, linear(x, a, b), label="expected linear")
plt.plot(x, a1_rational / (1 + b1_rational * x), label="predicted exhaustive")
plt.plot(x, a2_rational / (1 + b2_rational * x), label="predicted gauss")
plt.plot(x, a3_rational / (1 + b3_rational * x), label="predicted nelder")
plt.legend(loc="best")
plt.savefig("./plots/plot_disp_0_1.png")

plt.show()

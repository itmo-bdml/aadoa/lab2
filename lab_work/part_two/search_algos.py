import math
import numpy as np


def mse_loss(f: callable, a: float, b: float, x, predictions) -> float:
    l: float = 0
    for x_val, prediction in zip(x, predictions):
        l += (f(x_val, a, b) - prediction)**2
    return l


def exhaustive_search(f: callable, loss: callable, x, y, a_start: float = -1., a_finish: float = 1,
                      b_start: float = -1., b_finish: float = 1, step: float = 0.01):
    searched_a = np.arange(a_start, a_finish+step, step)
    searched_b = np.arange(b_start, b_finish+step, step)
    best_a = math.inf
    best_b = math.inf
    best_loss = math.inf
    iterations = 0
    for a in searched_a:
        for b in searched_b:
            cur_loss = loss(f, a, b, x, y)
            iterations += 1
            if cur_loss < best_loss:
                best_a = a
                best_b = b
                best_loss = cur_loss
    f_calculations = iterations
    return best_a, best_b, best_loss, iterations, f_calculations


# coordinate descent method
def gauss_method(f: callable, loss: callable, x, y, a_start: float = 0, a_finish: float = 1,
                      b_start: float = 0, b_finish: float = 1, epsilon: float = 0.001):
    cur_a = a_start
    cur_b = b_start
    steps = [(cur_a, cur_b)]
    loss_history = [loss(f, cur_a, cur_b, x, y)]
    iterations = 1
    f_calculations = 0
    while True:
        if iterations % 2:
            new_a, _, loss_value, _, calcs = exhaustive_search(f, loss, x, y, b_start=cur_b, b_finish=cur_b, step=0.001)
            if abs(cur_a - new_a) < epsilon:
                break
            cur_a = new_a
        else:
            _, new_b, loss_value, _, calcs = exhaustive_search(f, loss, x, y, a_start=cur_a, a_finish=cur_a, step=0.001)
            if abs(cur_b - new_b) < epsilon:
                break
            cur_b = new_b
        f_calculations += calcs
        if loss_history[-1] - loss_value < epsilon:
            break
        steps.append((cur_a, cur_b))
        loss_history.append(loss_value)
        iterations += 1
    steps.append((cur_a, cur_b))
    loss_history.append(loss)
    return cur_a, cur_b, loss_value, iterations, f_calculations

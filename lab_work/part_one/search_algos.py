import math
import numpy as np


def exhaustive_search(f: callable, a: float, b: float, epsilon: float = 0.001) \
        -> tuple[float, int, int, tuple[np.ndarray, np.ndarray]]:
    res: float = math.inf
    iterations: int = 0
    operations: int = 0
    x = np.arange(a, b+epsilon, epsilon)
    y = []
    for v in x:
        value = f(v)
        operations += 1
        if value < res:
            res = value
        y.append(value)
        iterations += 1
    y = np.array(y)
    return res, operations, iterations, (x, y)


def golden_section_search(f: callable, a: float, b: float, epsilon: float = 0.001)\
        -> tuple[float, int, int, tuple[np.ndarray, np.ndarray]]:
    golden_ration: float = (math.sqrt(5) + 1) / 2

    iterations: int = 0
    operations: int = 2
    x = []
    y = []
    c = b - (b - a) / golden_ration
    d = a + (b - a) / golden_ration
    while abs(f(c) - f(d)) > epsilon:
        fc = f(c)
        fd = f(d)
        operations += 2
        x.append((c, d))
        y.append((fc, fd))
        if fc < fd:
            b = d
        else:
            a = c
        c = b - (b - a) / golden_ration
        d = a + (b - a) / golden_ration
        iterations += 1
    res = f((c + d) / 2)
    operations += 1
    x = np.array(x)
    y = np.array(y)
    return res, operations, iterations, (x, y)


def dichotomy_search(f: callable, a: float, b: float, epsilon: float = 0.001, delta=0.0001)\
        -> tuple[float, int, int, tuple[np.ndarray, np.ndarray]]:

    iterations: int = 0
    operations: int = 0
    x = []
    y = []

    left, right = a, b
    lv, rv = f(a), f(b)
    operations += 2
    x.append((left, right))
    y.append((lv, rv))

    while (right - left) >= 2*epsilon:
        m = (right + left) / 2

        ml = f(m-delta)
        mr = f(m+delta)
        operations += 2
        if ml > mr:
            left = m-delta
            lv = ml
        else:
            right = m - delta
            rv = ml
        x.append((left, right))
        y.append((lv, rv))

        iterations += 1
    res = f((right+left) / 2)
    operations += 1
    x = np.array(x)
    y = np.array(y)
    return res, operations, iterations, (x, y)

import math
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
matplotlib.use('TkAgg')

import lab_work.part_one.search_algos as algos


def qubic_x(x: float) -> float:
    return x**3


def abs_x(x: float) -> float:
    return abs(x-0.2)


def third_f(x: float) -> float:
    return x * math.sin(1/x)


def get_x_y(f: callable, start: float = 0, finish: float = 0, step: float = 0.01) -> tuple[np.ndarray, np.ndarray]:
    x = np.arange(start, finish, step)
    y = np.fromiter(map(f, x), float)
    return x, y


algos_list = [algos.exhaustive_search, algos.dichotomy_search, algos.golden_section_search]
functions = [qubic_x, abs_x, third_f]
borders = [(0, 1), (0, 1), (0.01, 1)]

res, operations, iterations, (x, y) = algos.dichotomy_search(abs_x, 0, 1)

for func, border in zip(functions, borders):
    for algo in algos_list:

        res, operations, iterations, (x, y) = algo(func, border[0], border[1])
        print(algo, res, operations, iterations)

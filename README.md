## Lab 2
### Algorithms for unconstrained nonlinear optimization. Direct methods

---
[Report (google doc)](https://docs.google.com/document/d/103z73Q4C7WF64u4gn2trwkx779SrbR5SxvTts67h-10)

Performed by:
* Elya Avdeeva
* Gleb Mikloshevich

### Plots
Noise N(0, 1)

<img src="./lab_work/part_two/plots/plot.png" height="250">


Noise N(0, 0.05)

<img src="./lab_work/part_two/plots/plot_disp_0_1.png" height="250">

### Goal

The use of direct methods (one-dimensional methods of exhaustive search, dichotomy, golden section search; multidimensional methods of exhaustive search, Gauss (coordinate descent), Nelder-Mead) in the tasks of unconstrained nonlinear optimization.

### Formulation of the problem

**Part One**

Use the one-dimensional methods of exhaustive search, dichotomy and golden section search to find an approximate (with precision ε = 0.001) solution x: f(x) → min for the following functions and domains: 

1. x^3, x ∈ [0, 1]
2. abs(x-0.2), x ∈ [0, 1]
3. x*sin(1/x), x ∈ [0.01, 1]

Calculate the number of 𝑓-calculations and the number of iterations performed in
each method and analyze the results. Explain differences (if any) in the results
obtained.

**Part Two**

Generate random numbers 𝛼 ∈ (0,1) and 𝛽 ∈ (0,1). Furthermore, generate the
noisy data {𝑥 , 𝑦}, where 𝑘 = 0, … ,100, according to the following rule:
y = 𝛼x + 𝛽 + 𝛿, 𝛿 ~ 𝑁(0,1)

Approximate the data by the following linear and rational functions:
1. 𝐹 (𝑥, 𝑎, 𝑏) = 𝑎𝑥 + 𝑏 (linear approximant),
2. 𝐹 (𝑥, 𝑎, 𝑏) = 𝑎 / (1 + 𝑏𝑥) (rational approximant)

by means of least squares through the numerical minimization (with precision 𝜀 =
0.001)

To solve the minimization problem, use the methods of exhaustive search, Gauss and
Nelder-Mead. If necessary, set the initial approximations and other parameters of
the methods. Visualize the data and the approximants obtained in a plot separately
for each type of approximant so that one can compare the results for the numerical
methods used. Analyze the results obtained (in terms of number of iterations,
precision, number of function evaluations, etc.).